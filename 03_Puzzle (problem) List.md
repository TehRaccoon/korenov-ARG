# Puzzle ideas
- Linux terminal jako main information hub
	- dobre se ovlada "on the fly"
	- bude tam "text adventure"
	- zasifrovany soubory ktery se odsifrujou pomoci kodu (a s cool animaci)
	- actual programy ktery spustej remote devices, nebo FM transmitter
	- possibly lore
- najdou "data cube"
	1) je potreba ji napajet
		- bud powerpackem (kterej najdou) nebo hotwire
	2) musi se zadat kod do keypadu
		- keypad ma tlacitka 1-8, ale nejaky kody budou mit i cisla 0 a 9
		- aby slo zadat cisla 0 a 9, bude se muset odmontovat "service panel" zamcenej torxama
		- v service panelu bude nepajivy pole, nebo konektory a moznost zapojit drat cisla 0 a 9 na nejakou klavesu keypadu
	3) data cube ukaze data
# Puzzle devices
## Physical
- Locked box & broken key - requires lockpick.
- Torx-screwed box (torx bit is a "key") [[01_Buy list]]
	- possible to make with 4 unique keys (imbus, philips, flat, torx)
- Egg hunt
	- Chess board - pieces placed in positions based on a picture => gets you a code from symbols on bottom of the pieces -> 8x8 grid + symbols.
- **Schovat něco do hajzlu**
- UV light + fixa [[01_Buy list]]
- data cubes
	- they need to be powered somehow to get data from them
	- Electronic box with clue - needs power source - either "hotwire" with AA bateries, or "power source box/holder for AAs"
## Digital / crypto / info
- flash disk mount na linuxu
- ip webcam co ukazuje na neco dulezityho
- Plakát - Caesar or ROT13
- radio transmitter [[01_Buy list]]
- integrace gitlabu a pastebinu
- text adventure
## random ideas
- ~~Hot & cold cache (proximity sounds)~~ (too hard to make)
- Vysílačky - 2 pozice?
- Terminal [[01_Buy list]]
- Hlasová schránka message
- Velkej timer -> něco odpočítává?
- Timed event -> jednou za X času se na Y času spustí remote display s nápovědou.
- QR codes
	- venku
	- na chatě
- remote control ESP [[01_Buy list]]
- NFC nalepky [[01_Buy list]]
- Random quotes jako indície.
- keypad with a hotwire
	- maybe a variation of the data cube?
		- mohla by mit "service port" kde by slo bud hotwire, nebo prepojit draty, aby slo zadat cislo 9 nebo 0
- pareidolia
- kombinacni zamek s prenastavitelnym kodem
- zamek bez klice