from machine import Pin, SoftI2C
import ssd1306
from time import sleep

sleep(1)

# I2C config
i2c = SoftI2C(scl=Pin(1), sda=Pin(0))

# Display config
oled_width = 128
oled_height = 64
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

# Custom functions

def display_text(text):
    global oled
    oled.fill(0)
    oled.text("input code:", 0, 0)
    oled.text(text, 32, 10)
    oled.show()

def display_raw_text(text):
    global oled
    oled.fill(0)
    oled.text(text, 32, 10)
    oled.show()

# currently input code
code = ""

def check_code():
    global code
    if len(code) < 4:
        return
    else:
        if code == "1337":
            display_raw_text("nice")
            sleep(2)
        elif code == "":
            pass
        else:
            display_raw_text("wrong code")
            sleep(2)
    code = ""
    display_text(code)

def code_input(num):
    #global key_pressed
    global code
    global oled
    #key_pressed = True
    code = code + str(num)  # muze bejt optimalizovany kdyz dam string primo do switch-case
    display_text(code)
    block_keys()

def check_all_keys():
    out = key0.value() + key1.value() + key2.value() + key3.value() + key4.value() + key5.value() + key6.value() + key7.value() + key8.value() + key9.value()
    if out == 0:
        return False
    elif out > 0:
        return True
    else:
        raise ValueError("amount of pressed keys is less than zero")
    
def block_keys():
    while check_all_keys():
        pass
    else:
        check_code()

#### keypad here

#push_button = Pin(28, Pin.IN)
#led = Pin("LED", Pin.OUT)
key0 = Pin(16, Pin.IN, Pin.PULL_DOWN)
key1 = Pin(18, Pin.IN, Pin.PULL_DOWN)
key2 = Pin(19, Pin.IN, Pin.PULL_DOWN)
key3 = Pin(11, Pin.IN, Pin.PULL_DOWN)
key4 = Pin(20, Pin.IN, Pin.PULL_DOWN)
key5 = Pin(12, Pin.IN, Pin.PULL_DOWN)
key6 = Pin(13, Pin.IN, Pin.PULL_DOWN)
key7 = Pin(15, Pin.IN, Pin.PULL_DOWN)
key8 = Pin(14, Pin.IN, Pin.PULL_DOWN)
key9 = Pin(17, Pin.IN, Pin.PULL_DOWN)

display_text(code)

while True:
    if key0.value() == 1:
        code_input(0)
    elif key1.value() == 1:
        code_input(1)
    elif key2.value() == 1:
        code_input(2)
    elif key3.value() == 1:
        code_input(3)
    elif key4.value() == 1:
        code_input(4)
    elif key5.value() == 1:
        code_input(5)
    elif key6.value() == 1:
        code_input(6)
    elif key7.value() == 1:
        code_input(7)
    elif key8.value() == 1:
        code_input(8)
    elif key9.value() == 1:
        code_input(9)
    else:
        pass

